-- |
-- Project : SIMPLIFY-BKG
-- Module  : CfgData
-- Author  : Daniel Haris
-- Login   : xharis00

module CfgData where

-- | Grammar rule type definition
type Rule = (Char, [Char])
-- | Context-free grammar data structure
data CFG = CFG [Char] [Char] Char [Rule]
