-- |
-- Project : SIMPLIFY-BKG
-- Module  : CfgParser
-- Author  : Daniel Haris
-- Login   : xharis00

module CfgParser where

-- | Imports
import Text.ParserCombinators.Parsec hiding ((<|>), many)
import Control.Applicative
import CfgData

-- | Monadic parser of the input file
-- 
-- Inspired from <https://github.com/aslatter/parsec>
non_terminals :: Parser String
non_terminals = do
    ns <- (upper `sepBy1` char ',' )
    char '\n'
    return ns

terminals :: Parser String
terminals = do
    ts <- try(lower `sepBy1` (char ',')) <|> string ""
    char '\n'
    return ts

start_symbol :: Parser Char
start_symbol = do
    ss <- upper
    char '\n'
    return ss

rule :: Parser (Char, String)
rule = do
    l <- upper
    string "->"
    r <- many1 letter <|> string "#"
    char '\n'
    return (l,r)

rules :: Parser [(Char, String)]
rules = do
    rs <- many1 rule
    return rs

cfg_parser :: Parser Bool
cfg_parser = do
    non_terminals
    terminals
    start_symbol
    rules
    return True

-- | Function syntactically validates the input content with a monadic parser
isSyntacticallyValidCFG :: Parser Bool -> String -> Bool
isSyntacticallyValidCFG p input = case (parse p "" input) of
                              Left _ -> False
                              Right True -> True

-- | Function validates the input CFG
-- [@ss_in_ns@] Start symbol is 1 of non terminals
-- [@ss_rule@] At least 1 rule with the start symbol
-- [@rs_ok@]
--     * L side of rule must be 1 of non terminals
--     * R side of the rule must contain symbols from (N ++ T) or it is an epsilon rule
--     * if no terminals, must be at least 1 epsilon rule
--     * if no terminals, must not be any other rule than epsilon rule
isSemanticallyValidCFG :: CFG -> Bool
isSemanticallyValidCFG (CFG ns ts ss rs) = ss_in_ns && ss_rule && (rs_ok || eps_rs_ok)
    where
        ss_in_ns = elem ss ns
        ss_rule = any (\(l,_) -> ss == l) rs
        eps_rs_ok = null ts && all (\(l,r) -> elem l ns && r == "#") rs
        rs_ok = all (\(l,r) -> elem l ns && (all (\r_sym -> elem r_sym $ ns ++ ts) r || r == "#")) rs
