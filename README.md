## Projekt č.1 do premetu FLP
## Autor: Daniel Haris
## Zadanie: SIMPLIFY-BKG - odstránenie zbytočných symbolov bezkontextovej gramatiky
## Dátum: 31.3.2017

Na odstránenie zbytočných symbolov bol použitý algoritmus prezentovaný v predmete TIN (4.1).
1. Program načíta vstupnú bezkontextovú gramatiku do internej reprezentácie, pričom sa skontroluje syntax a sémantika. Pomocou parametru -i vypíšeme gramatiku v nezmenenej forme.
2. Vypočíta sa množina Nt a z pôvodnej gramatiky sa odstránia neterminály, ktoré negenerujú terminálne reťazce a všetky ich pravidlá. na základe algorimu. Upravená gramatika v tomto kroku sa vypíše pomocou parametru -1.
3. Vypočíta sa množina V a z pôvodnej gramatiky sa odstránia nedosiahnuteľné symboly. Upravená gramatika sa vypíše pomocou paramteru -2.

K programu bol vytvorený testovací skript "test.sh", ktorý obsahuje 31 testov rôzneho druhu (parametrické, syntaktické, sémantické a algoritmické). Skript opakovane spúšťa program s testovacími vstupmi z adresáru "tests/".

Návod na spustenie programu:
- make
- ./simplify-bkg [volby] [vstup]
- make test
- make clean

Stručný popis jednotlivých modulov:
- Main.hs - hlavný program
- CfgParser.hs - obsahuje monadický parser na validáciu vstupov podľa syntaxe
- CfgData.hs - dátové typy a štruktúry
- Algorithms.sh - algoritmus eliminácie zbytočných symbolov
