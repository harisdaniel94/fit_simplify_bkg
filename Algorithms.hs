-- |
-- Project : SIMPLIFY-BKG
-- Module  : Algorithms
-- Author  : Daniel Haris
-- Login   : xharis00

module Algorithms where

import Data.Char
import Data.List
import CfgData

-- | Function calculates 1 step in Nt algorithm
ntStep :: CFG -> String -> String
ntStep (CFG ns ts _ rs) prev_nt = [nt | nt    <- ns,
                                        (_,r) <- filter (\(l,_) -> l == nt) rs,
                                        all (\x -> elem x (prev_nt ++ ts)) r]

-- | Function returning list of non-terminals generating terminal symbols
calcNt :: CFG -> String
calcNt (CFG ns ts ss rs) = go []
    where go xs = let xs' = nub $ xs ++ (ntStep (CFG ns ts ss rs) xs)
                  in if xs == xs'
                        then xs
                        else go xs'

-- | Function calculates 1 step in V algorithm
vStep :: CFG -> String -> String
vStep (CFG _ _ _ rs) prev_v = prev_v ++ [x | n     <- filter (isUpper) prev_v,
                                             (_,r) <- filter (\(l,_) -> l == n) rs,
                                             x     <- r]

-- | Function returning list of reachable symbols
calcV :: CFG -> String
calcV (CFG ns ts ss rs) = go [ss]
    where go xs = let xs' = nub $ xs ++ (vStep (CFG ns ts ss rs) xs)
                  in if xs == xs'
                        then xs
                        else go xs'
