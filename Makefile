# compile
all:
	ghc Main.hs -o simplify-bkg

# run tests
test:
	chmod +x test.sh
	bash test.sh

# clean folder
clean:
	rm simplify-bkg *.o *.hi
