-- |
-- Project : SIMPLIFY-BKG
-- Module  : Main
-- Author  : Daniel Haris
-- Login   : xharis00

module Main(main) where

-- | Imports
import System.Environment
import Data.List
import Data.List.Split
import CfgData
import CfgParser
import Algorithms

-- | Function parses cmdline arguments
parseArgs :: [String] -> (Bool, Bool, Bool, String)
parseArgs [] = error "No args. Usage ./simplify-bkg {-i|-1|-2} [input_file]"
parseArgs [a]
    | a == "-i" = (True, False, False, "stdin")
    | a == "-1" = (False, True, False, "stdin")
    | a == "-2" = (False, False, True, "stdin")
    | otherwise = error "Invalid args. Usage ./simplify-bkg {-i|-1|-2} [input_file]"
parseArgs [a,b]
    | a == "-i" = (True, False, False, b)
    | a == "-1" = (False, True, False, b)
    | a == "-2" = (False, False, True, b)
    | otherwise = error "Invalid args. Usage ./simplify-bkg {-i|-1|-2} [input_file]"
parseArgs _ = error "Invalid args. Usage ./simplify-bkg {-i|-1|-2} [input_file]"

-- | Function prints a context free grammar
cfgToStr :: CFG -> String
cfgToStr (CFG ns ts ss rs) = ns' ++ ts' ++ ss' ++ rs'
    where
        ns' = intersperse ',' ns ++ "\n"
        ts' = intersperse ',' ts ++ "\n"
        ss' = ss : "\n"
        rs' = foldr (++) "" (map (\(l,r) -> l : "->" ++ r ++ "\n") rs) 

-- | Function parses input cfg into internal representation
getG0 :: [String] -> CFG
getG0 [] = error "Input file is empty."
getG0 (ns:ts:ss:rs) = (CFG ns' ts' ss' rs')
    where
        ns' = [n | n <- filter (/= ',') ns]
        ts' = [t | t <- filter (/= ',') ts]
        ss' = head ss
        raw_rules = filter (not.null) (map (splitOn "->") rs)
        rs' = zip (map (head.head) raw_rules) (map (head.tail) raw_rules)
getG0 _ = error "Input file content does not satisfy the format."

-- | Function eliminates non terminals not generating terminals symbols
-- in given CFG and returns the updated CFG
getG1 :: CFG -> String -> CFG
getG1 (CFG _ ts ss rs) nt = (CFG ns' ts ss rs')
    where
        ns' = nub $ ss : nt
        rs' = filter (\(l,r) -> elem l nt && all (\p -> elem p (nt ++ ts)) r) rs

-- | Function eliminates unreachable symbols in given CFG and returns the updated CFG
getG2 :: CFG -> String -> String -> CFG
getG2 (CFG _ ts ss rs) nt v = (CFG ns' ts' ss rs')
    where
        ns' = filter (\x -> elem x v) nt
        ts' = filter (\x -> elem x v) ts
        rs' = filter (\(l,r) -> elem l ns' && all (\p -> elem p v) r) rs

-- | Main function
main :: IO ()
main = do
    -- parse cmdline arguments
    args <- getArgs
    let (f_i, f_1, _, file_path) = parseArgs args
    -- get input file content
    raw_cfg <- if file_path == "stdin"
                   then getContents
                   else readFile file_path
    -- check input CFG syntax
    if isSyntacticallyValidCFG cfg_parser raw_cfg
        -- syntax is correct so load input CFG into CFG data structure
        then let cfg_i = getG0 $ lines raw_cfg
             -- validate input CFG's semantic asserts
             in if isSemanticallyValidCFG cfg_i
                    -- compute sets Nt and V and eliminate useless symbols
                    then let nt    = calcNt $ cfg_i
                             cfg_1 = getG1 cfg_i nt
                             v     = calcV cfg_1
                             cfg_2 = getG2 cfg_1 nt v
                         -- print the specified step in the elimination process
                         in if f_i
                            then putStr.cfgToStr $ cfg_i
                            else if f_1
                                then putStr.cfgToStr $ cfg_1
                                else putStr.cfgToStr $ cfg_2
                    -- semantic error detected
                    else error "Invalid semantics."
        -- parser detected a syntax error
        else error "Invalid syntax."
    return ()
