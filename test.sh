#!/usr/bin/env bash

# Project: FLP Project in Haskell - Simplify CFG
# Name: Daniel Haris
# Login: xharis00
# File: test.sh

INT=ghc
EXT=hs
SIMPLIFY_BKG=simplify-bkg

PASS="[ \033[0;32mPASS\033[0;0m ]"
FAIL="[ \033[0;31mFAIL\033[0;0m ]"
bold=$(tput bold)
normal=$(tput sgr0)

echo "#########################"
echo "##### ARGUMENT TEST #####"
echo "#########################"

echo -n "01 ${bold}no arg${normal}               >> "
./$SIMPLIFY_BKG 2> /dev/null
if [ $? == 1 ]; then printf "$PASS\n"; else printf "$FAIL\n"; fi

echo -n "02 ${bold}2 flags${normal}              >> "
./$SIMPLIFY_BKG -i -1 2> /dev/null
if [ $? == 1 ]; then printf "$PASS\n"; else printf "$FAIL\n"; fi

echo -n "03 ${bold}3 flags${normal}              >> "
./$SIMPLIFY_BKG -i -1 -2 2> /dev/null
if [ $? == 1 ]; then printf "$PASS\n"; else printf "$FAIL\n"; fi

echo "##########################"
echo "##### SYNTACTIC TEST #####"
echo "##########################"

echo -n "04 ${bold}empty file${normal}           >> "
./$SIMPLIFY_BKG -i tests/empty_file.in 2> /dev/null
if [ $? == 1 ]; then printf "$PASS\n"; else printf "$FAIL\n"; fi

echo -n "05 ${bold}lorem ipsum file${normal}     >> "
./$SIMPLIFY_BKG -i tests/lorem_ipsum.in 2> /dev/null
if [ $? == 1 ]; then printf "$PASS\n"; else printf "$FAIL\n"; fi

echo -n "06 ${bold}only non terminals${normal}   >> "
./$SIMPLIFY_BKG -i tests/only_non_terminals.in 2> /dev/null
if [ $? == 1 ]; then printf "$PASS\n"; else printf "$FAIL\n"; fi

echo -n "07 ${bold}only terminals${normal}       >> "
./$SIMPLIFY_BKG -i tests/only_terminals.in 2> /dev/null
if [ $? == 1 ]; then printf "$PASS\n"; else printf "$FAIL\n"; fi

echo -n "08 ${bold}only start symbol${normal}    >> "
./$SIMPLIFY_BKG -i tests/only_start_symbol.in 2> /dev/null
if [ $? == 1 ]; then printf "$PASS\n"; else printf "$FAIL\n"; fi

echo -n "09 ${bold}only 1 rule${normal}          >> "
./$SIMPLIFY_BKG -i tests/only_1_rule.in 2> /dev/null
if [ $? == 1 ]; then printf "$PASS\n"; else printf "$FAIL\n"; fi

echo -n "10 ${bold}no non terminals${normal}     >> "
./$SIMPLIFY_BKG -i tests/no_non_terminals.in 2> /dev/null
if [ $? == 1 ]; then printf "$PASS\n"; else printf "$FAIL\n"; fi

echo -n "11 ${bold}start symbol in T${normal}    >> "
./$SIMPLIFY_BKG -i tests/start_symbol_in_t.in 2> /dev/null
if [ $? == 1 ]; then printf "$PASS\n"; else printf "$FAIL\n"; fi

echo -n "12 ${bold}no rules${normal}             >> "
./$SIMPLIFY_BKG -i tests/no_rules.in 2> /dev/null
if [ $? == 1 ]; then printf "$PASS\n"; else printf "$FAIL\n"; fi

echo -n "13 ${bold}wrong commas${normal}         >> "
./$SIMPLIFY_BKG -i tests/wrong_commas.in 2> /dev/null
if [ $? == 1 ]; then printf "$PASS\n"; else printf "$FAIL\n"; fi

echo -n "14 ${bold}extra whitespaces${normal}    >> "
./$SIMPLIFY_BKG -i tests/extra_whitespace.in 2> /dev/null
if [ $? == 1 ]; then printf "$PASS\n"; else printf "$FAIL\n"; fi

echo -n "15 ${bold}lower letter in N${normal}    >> "
./$SIMPLIFY_BKG -i tests/lower_letter_in_n.in 2> /dev/null
if [ $? == 1 ]; then printf "$PASS\n"; else printf "$FAIL\n"; fi

echo -n "16 ${bold}upper letter in T${normal}    >> "
./$SIMPLIFY_BKG -i tests/upper_letter_in_t.in 2> /dev/null
if [ $? == 1 ]; then printf "$PASS\n"; else printf "$FAIL\n"; fi

echo -n "17 ${bold}lower letter in S${normal}    >> "
./$SIMPLIFY_BKG -i tests/lower_letter_in_s.in 2> /dev/null
if [ $? == 1 ]; then printf "$PASS\n"; else printf "$FAIL\n"; fi

echo -n "18 ${bold}lower R-value rule${normal}   >> "
./$SIMPLIFY_BKG -i tests/lower_r-val_rule.in 2> /dev/null
if [ $? == 1 ]; then printf "$PASS\n"; else printf "$FAIL\n"; fi

echo -n "19 ${bold}empty R-value rule${normal}   >> "
./$SIMPLIFY_BKG -i tests/empty_r-val_rule.in 2> /dev/null
if [ $? == 1 ]; then printf "$PASS\n"; else printf "$FAIL\n"; fi

echo -n "20 ${bold}double epsilon rule${normal}  >> "
./$SIMPLIFY_BKG -i tests/double_epsilon.in 2> /dev/null
if [ $? == 1 ]; then printf "$PASS\n"; else printf "$FAIL\n"; fi

echo -n "21 ${bold}epsilon in terminals${normal} >> "
./$SIMPLIFY_BKG -i tests/epsilon_in_t.in 2> /dev/null
if [ $? == 1 ]; then printf "$PASS\n"; else printf "$FAIL\n"; fi

echo -n "22 ${bold}special character${normal}    >> "
./$SIMPLIFY_BKG -i tests/special_character.in 2> /dev/null
if [ $? == 1 ]; then printf "$PASS\n"; else printf "$FAIL\n"; fi

echo "#########################"
echo "##### SEMANTIC TEST #####"
echo "#########################"

echo -n "23 ${bold}L-value not in N${normal}     >> "
./$SIMPLIFY_BKG -i tests/l-val_not_in_n.in 2> /dev/null
if [ $? == 1 ]; then printf "$PASS\n"; else printf "$FAIL\n"; fi

echo -n "24 ${bold}R-value not in T${normal}     >> "
./$SIMPLIFY_BKG -i tests/r-val_not_in_t.in 2> /dev/null
if [ $? == 1 ]; then printf "$PASS\n"; else printf "$FAIL\n"; fi

echo -n "25 ${bold}R-value not in N${normal}     >> "
./$SIMPLIFY_BKG -i tests/r-val_not_in_n.in 2> /dev/null
if [ $? == 1 ]; then printf "$PASS\n"; else printf "$FAIL\n"; fi

echo -n "26 ${bold}S not in N${normal}           >> "
./$SIMPLIFY_BKG -i tests/s_not_in_n.in 2> /dev/null
if [ $? == 1 ]; then printf "$PASS\n"; else printf "$FAIL\n"; fi

echo "##########################"
echo "##### ALGORITHM TEST #####"
echo "##########################"

echo -n "27 ${bold}1st CFG -i${normal}           >> "
./$SIMPLIFY_BKG -i tests/simplify1.in > tests/simplify1_i.out
diff "tests/simplify1_ref_i.out" "tests/simplify1_i.out" 2> /dev/null
if [ $? == 0 ]; then printf "$PASS\n"; else printf "$FAIL\n"; fi
echo -n "           ${bold}-1${normal}           >> "
./$SIMPLIFY_BKG -1 tests/simplify1.in > tests/simplify1_1.out 
diff "tests/simplify1_ref_1.out" "tests/simplify1_1.out" 2> /dev/null
if [ $? == 0 ]; then printf "$PASS\n"; else printf "$FAIL\n"; fi
echo -n "           ${bold}-2${normal}           >> "
./$SIMPLIFY_BKG -2 tests/simplify1.in > tests/simplify1_2.out
diff "tests/simplify1_ref_2.out" "tests/simplify1_2.out" 2> /dev/null
if [ $? == 0 ]; then printf "$PASS\n"; else printf "$FAIL\n"; fi

echo -n "28 ${bold}2nd CFG -i${normal}           >> "
./$SIMPLIFY_BKG -i tests/simplify2.in > tests/simplify2_i.out
diff "tests/simplify2_ref_i.out" "tests/simplify2_i.out" 2> /dev/null
if [ $? == 0 ]; then printf "$PASS\n"; else printf "$FAIL\n"; fi
echo -n "           ${bold}-1${normal}           >> "
./$SIMPLIFY_BKG -1 tests/simplify2.in > tests/simplify2_1.out
diff "tests/simplify2_ref_1.out" "tests/simplify2_1.out" 2> /dev/null
if [ $? == 0 ]; then printf "$PASS\n"; else printf "$FAIL\n"; fi
echo -n "           ${bold}-2${normal}           >> "
./$SIMPLIFY_BKG -2 tests/simplify2.in > tests/simplify2_2.out
diff "tests/simplify2_ref_2.out" "tests/simplify2_2.out" 2> /dev/null
if [ $? == 0 ]; then printf "$PASS\n"; else printf "$FAIL\n"; fi

echo -n "29 ${bold}3rd CFG -i${normal}           >> "
./$SIMPLIFY_BKG -i tests/simplify3.in > tests/simplify3_i.out
diff "tests/simplify3_ref_i.out" "tests/simplify3_i.out" 2> /dev/null
if [ $? == 0 ]; then printf "$PASS\n"; else printf "$FAIL\n"; fi
echo -n "           ${bold}-1${normal}           >> "
./$SIMPLIFY_BKG -1 tests/simplify3.in > tests/simplify3_1.out
diff "tests/simplify3_ref_1.out" "tests/simplify3_1.out" 2> /dev/null
if [ $? == 0 ]; then printf "$PASS\n"; else printf "$FAIL\n"; fi
echo -n "           ${bold}-2${normal}           >> "
./$SIMPLIFY_BKG -2 tests/simplify3.in > tests/simplify3_2.out
diff "tests/simplify3_ref_2.out" "tests/simplify3_2.out" 2> /dev/null
if [ $? == 0 ]; then printf "$PASS\n"; else printf "$FAIL\n"; fi

echo -n "30 ${bold}4th CFG -i${normal}           >> "
./$SIMPLIFY_BKG -i tests/simplify4.in > tests/simplify4_i.out
diff "tests/simplify4_ref_i.out" "tests/simplify4_i.out" 2> /dev/null
if [ $? == 0 ]; then printf "$PASS\n"; else printf "$FAIL\n"; fi
echo -n "           ${bold}-1${normal}           >> "
./$SIMPLIFY_BKG -1 tests/simplify4.in > tests/simplify4_1.out
diff "tests/simplify4_ref_1.out" "tests/simplify4_1.out" 2> /dev/null
if [ $? == 0 ]; then printf "$PASS\n"; else printf "$FAIL\n"; fi
echo -n "           ${bold}-2${normal}           >> "
./$SIMPLIFY_BKG -2 tests/simplify4.in > tests/simplify4_2.out
diff "tests/simplify4_ref_2.out" "tests/simplify4_2.out" 2> /dev/null
if [ $? == 0 ]; then printf "$PASS\n"; else printf "$FAIL\n"; fi

echo -n "31 ${bold}5th CFG -i${normal}           >> "
./$SIMPLIFY_BKG -i tests/simplify5.in > tests/simplify5_i.out
diff "tests/simplify5_ref_i.out" "tests/simplify5_i.out" 2> /dev/null
if [ $? == 0 ]; then printf "$PASS\n"; else printf "$FAIL\n"; fi
echo -n "           ${bold}-1${normal}           >> "
./$SIMPLIFY_BKG -1 tests/simplify5.in > tests/simplify5_1.out
diff "tests/simplify5_ref_1.out" "tests/simplify5_1.out" 2> /dev/null
if [ $? == 0 ]; then printf "$PASS\n"; else printf "$FAIL\n"; fi
echo -n "           ${bold}-2${normal}           >> "
./$SIMPLIFY_BKG -2 tests/simplify5.in > tests/simplify5_2.out
diff "tests/simplify5_ref_2.out" "tests/simplify5_2.out" 2> /dev/null
if [ $? == 0 ]; then printf "$PASS\n"; else printf "$FAIL\n"; fi
